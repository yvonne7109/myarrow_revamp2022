# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

### This repo is for the MyArrow Dashboard Revamp 2022 ###

* 1.email_report_billto_association.bq is the email report table with the billto association logic on BQ
* 2.activity_billto_association.bq is the GA activity table with the billto association logic on BQ
* 3.revamp_revenue_association.hql is the revenue table with the billto association logic on Cluster
* 4.billto_activity.bq is the GA activity table on BQ, it's for the User Row Data tab in the dashboard
* 5.event_level_stats.bq is the GA activity table on BQ, it's for the GA and Usage tab in the dashboard
* 6.fiscal_ref_5q.hql is the rolling 5 quarters for fiscal calendar on Cluster, which has been applied to the revamp_revenue_association table