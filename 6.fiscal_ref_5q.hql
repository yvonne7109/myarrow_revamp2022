
/* 
Theme: MyArrow Revamp
Project name: fiscal ref 5q
Query number: #3
Query name: created fiscal ref 5q in cluster
Date: Mar/21/2022
Author: Niraj
*/

Create table myarrow.fiscal_ref_5q as 
WITH 5q_data as(
    select 
        * 
    from(
        select 
            order_fiscal_quarter,
            rank() over( order by order_fiscal_quarter desc )  as ran 
        from 
            (select distinct 
                order_fiscal_quarter 
            from 
                arrowdotcom_reporting.fiscal_ref 
            where date_time <= now()) d) d1
        where ran <=6 )

select 
    * 
from 
    arrowdotcom_reporting.fiscal_ref 
where 
    order_fiscal_quarter in 
    (select 
        order_fiscal_quarter 
    from 
        5q_data);