/* 
Theme: MyArrow Revamp
Project name: Billto association logic application
Query number: #1
Query name: Billto association for email report
Date: Mar/21/2022
Author: Yvonne, Niraj 
*/


create or replace table `arrow-bigquery.myarrow_dev_revamp.email_report_billto_association` as
-- Get distinct records in billto table
with uiq as(
    select distinct
        unity_billto,
        billto_adj
    from `arrow-bigquery.myarrow_dev.billto_association`),

-- Inner join email table on unity billto, to have value for billto adj
-- Create actual active tag to mark all these billtos as non-actual active for now
list1 as(
    select distinct 
        uiq.billto_adj as account_number__c,
        exe.fiscal_month,
        exe.fiscal_quarter,
        0 as actual_active
    from 
        uiq
    inner join 
        `arrow-bigquery.myarrow_dev.myarrow_executive_email` exe
    on exe.account_number__c = uiq.unity_billto),

-- Inner join email table on billto adj, to have value for unity billto
-- Create actual active tag to mark all these billtos as non-actual active for now
list2 as(
    select distinct 
        uiq.unity_billto as account_number__c,
        exe.fiscal_month,
        exe.fiscal_quarter,
        0 as actual_active
    from 
        uiq
    inner join 
        `arrow-bigquery.myarrow_dev.myarrow_executive_email` exe
    on exe.account_number__c = uiq.billto_adj),

-- Clean up the email table, keeps only billto, fiscal month, fiscal quarter
-- Create actual active tag to mark all these billtos as actual active
exe as (
    select distinct
        account_number__c,
        fiscal_month,
        fiscal_quarter,
        1 as actual_active
    from `arrow-bigquery.myarrow_dev.myarrow_executive_email`),

-- Union list1, list2 and email table
com as(
    select 
        *
    from 
        list1
    union distinct 
    select 
        *
    from 
        list2
    union distinct 
    select 
        *
    from 
        exe)

-- Get the correct tag for billto: if the billto has actual active in that month, then actual active
select 
    account_number__c,
    fiscal_month,
    fiscal_quarter,
    max(actual_active) as actual_active,
    1 as final_active
from 
    com    
where fiscal_quarter in 
    (select 
        order_fiscal_quarter 
    from 
        `arrow-bigquery.reference.fiscal_ref_5q`)    
group by 1,2,3;





/* Validation Test*/

-----Test 1-----

-- Retrive the non-actual active billtos
with cte as(
    select distinct 
        account_number__c,
        fiscal_month,
        fiscal_quarter,
        actual_active
    from 
        `arrow-bigquery.myarrow_dev_revamp.email_report_billto_association` 
    where actual_active = 0),

-- Inner join the email table
cte2 as(
    select 
        exe.account_number__c,
        exe.fiscal_month as ori_month,
        cte.fiscal_month as adj_month
    from 
        cte
    inner join 
        `arrow-bigquery.myarrow_dev.myarrow_executive_email` exe
    on exe.account_number__c = cte.account_number__c)


-- For the same month, test if the non-actual active billtos are in the email table
select 
    *
from 
    cte2 
where ori_month = adj_month;


-----Test 2-----

-- Find the billtos have association
select
    distinct 
    exe.account_number__c,
    bill.billto_adj,
    exe.fiscal_month
from 
    `arrow-bigquery.myarrow_dev.myarrow_executive_email` exe
inner join 
    `arrow-bigquery.myarrow_dev.billto_association` bill
on exe.account_number__c = bill.unity_billto;


-- Select one of them as an example, see the actual_active is 0 or 1
select 
    *
from 
    `arrow-bigquery.myarrow_dev_revamp.email_report_billto_association`
where account_number__c = "AN-2197762"
and fiscal_month = 202003;


-- If the actual_active is 0 then see if this billto is in the original activity table
-- It should not in the original table
select 
    *
from 
    `arrow-bigquery.myarrow_dev.myarrow_executive_email`
where account_number__c = "AN-2197762"
and fiscal_month = 202003;



select * from  reference.fiscal_ref_5q 
where order_fiscal_quarter <> posting_fiscal_quarter