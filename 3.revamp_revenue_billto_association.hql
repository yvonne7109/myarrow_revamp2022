/* 
Theme: MyArrow Revamp
Project name: Billto association logic application
Query number: #3
Query name: Billto association for revenue`
Date: Mar/30/2022
Author: Niraj, Yvonne
*/

create table myarrow.revamp_revenue_billto_association as 
-- Get distinct records in billto table
with uiq as(
    select distinct
        unity_billto,
        billto_adj
    from 
        arrowdotcom_reporting.billto_association),
	
-- Create actual active tag to mark all these billtos as actual active for both myarrow inc and myarrow bill to
revenue_prep as(
    select 
        unity_billto, 
        fiscal_month,
        fiscal_quarter,
        sum(revenue) as whole_revenue,
        sum(case when order_type = 'Online' then revenue else 0 end) as online_revenue,
        sum(case when order_type = 'Offline' then revenue else 0 end) as offline_revenue
    from 
        myarrow.myarrow_executive_dashboard_activity 
    group by 1,2,3),


revenue_data as(
    select 
        unity_billto, 
        fiscal_month,
        fiscal_quarter,
        whole_revenue,
        online_revenue,
        offline_revenue,
        if(whole_revenue >= 1, 1, 0) as actual_arrow_inc_active,
        if(online_revenue >= 1, 1, 0) as actual_myarrow_active
    from 
        revenue_prep),


-- Create actual active tag to mark all these billtos as non-actual active for now	
list1 as(
    select distinct 
        uiq.billto_adj as unity_billto,
        exe.fiscal_month,
        exe.fiscal_quarter,
        null as whole_revenue,
        null as online_revenue,
        null as offline_revenue,
        0 as actual_arrow_inc_active,
        0 as actual_myarrow_active
    from 
        uiq
    inner join 
       revenue_data exe
    on exe.unity_billto = uiq.unity_billto),
	
-- Create actual active tag to mark all these billtos as non-actual active for now	
list2 as(
    select distinct 
        uiq.unity_billto as unity_billto,
        exe.fiscal_month,
        exe.fiscal_quarter,
        null as whole_revenue,
        null as online_revenue,
        null as offline_revenue,
        0 as actual_arrow_inc_active,
        0 as actual_myarrow_active
    from 
        uiq
    inner join 
        revenue_data exe
    on exe.unity_billto = uiq.billto_adj),
    

-- Union list1, list2 and email table	
com as(
    select 
        *
    from 
        list1
    union distinct 
    select 
        *
    from 
        list2
    union distinct 
    select 
        *
    from 
        revenue_data)
        


-- Get the correct tag for billto: if the billto has actual active in that month, then actual active else it will be final active based on revenue
select 
    unity_billto,
    fiscal_month,
    fiscal_quarter,
    max(whole_revenue) as inc_revenue,
    max(online_revenue) as online_revenue,
    max(offline_revenue) as offline_revenue,
    max(actual_arrow_inc_active) as actual_arrow_inc_active,
    max(actual_myarrow_active) as actual_myarrow_active,
    case when 
        (max(whole_revenue) >= 1 or max(online_revenue) >= 1)  then 1
        else 0
        end as final_arrow_inc_active,
    case when 
        max(online_revenue) >= 1 then 1
        else 0
        end as final_myarrow_active    
from 
    com
where fiscal_quarter in 
    (select 
        order_fiscal_quarter 
    from 
        myarrow.fiscal_5q)
group by 1,2,3;



/* Validation Test*/

-----Test 1-------Test if the actual_arrow_inc_active is correct

--Retrive the non-actual active billtos
cte as(
    select 
        *
    from 
        final
    where actual_arrow_inc_active = 0),

--Prepare the original revenue table
ori as(
    select
        unity_billto,
        fiscal_month,
        sum(revenue) as revenue
    from 
        myarrow.myarrow_executive_dashboard_activity
    group by 1,2),

--Inner join the original revenue table
cte2 as(
    select 
        cte.unity_billto,
        ori.fiscal_month as ori_month,
        cte.fiscal_month as adj_month
    from 
        cte
    inner join 
        ori
    on ori.unity_billto = cte.unity_billto
    where ori.revenue >= 1 )

-- For the same month, test if the non-actual active billtos are in the revenue table
select 
    *
from 
    cte2 
where ori_month = adj_month;



-----Test 2-------Test if the actual_myarrow_active is correct

--Retrive the non-actual active billtos
cte as(
    select 
        *
    from 
        final
    where actual_myarrow_active = 0),

--Prepare the original revenue table
ori as(
    select
        unity_billto,
        fiscal_month,
        sum(revenue) as revenue
    from 
        myarrow.myarrow_executive_dashboard_activity
    where order_type = "Online"
    group by unity_billto,fiscal_month),

--Inner join the original revenue table
cte2 as(
    select 
        cte.unity_billto,
        ori.fiscal_month as ori_month,
        cte.fiscal_month as adj_month
    from 
        cte
    inner join 
        ori
    on ori.unity_billto = cte.unity_billto
    where ori.revenue >= 1 )

-- For the same month, test if the non-actual active billtos are in the revenue table
select 
    *
from 
    cte2 
where ori_month = adj_month;
