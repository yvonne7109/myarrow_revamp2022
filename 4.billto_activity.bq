CREATE TEMP FUNCTION
  has_registration_event(event_cat STRING,
    event_act STRING) AS ((
    SELECT
      CASE
        WHEN STRPOS(UPPER(event_cat), UPPER('Register')) > 0 AND STRPOS(UPPER(event_act), UPPER('Request')) > 0 THEN 1
        WHEN STRPOS(UPPER(event_cat), UPPER('Register')) > 0
      AND STRPOS(UPPER(event_act), UPPER('Success')) > 0 THEN 1
      ELSE
      0
    END
      ) );
CREATE TEMP FUNCTION
  has_quote_event(event_cat STRING,
    event_act STRING) AS ((
    SELECT
      CASE
        WHEN STRPOS(UPPER(event_cat), UPPER('Quote')) > 0 AND STRPOS(UPPER(event_act), UPPER('Add To Quote')) > 0 THEN 1
        WHEN STRPOS(UPPER(event_cat), UPPER('Quote')) > 0 AND STRPOS(UPPER(event_act), UPPER('Quote Request')) > 0 THEN 1
        WHEN STRPOS(UPPER(event_cat), UPPER('Quote')) > 0 AND STRPOS(UPPER(event_act), UPPER('Upload Quote')) > 0 THEN 1
      ELSE
      0
    END
      ) );
CREATE TEMP FUNCTION
  has_bom_event(event_cat STRING,
    event_act STRING) AS ((
    SELECT
      CASE
        WHEN STRPOS(UPPER(event_cat), UPPER('BOM')) > 0 AND STRPOS(UPPER(event_act), UPPER('Upload Existing')) > 0 THEN 1
        WHEN STRPOS(UPPER(event_cat), UPPER('BOM')) > 0
      AND STRPOS(UPPER(event_act), UPPER('Upload File')) > 0 THEN 1
        WHEN STRPOS(UPPER(event_cat), UPPER('BOM')) > 0 AND STRPOS(UPPER(event_act), UPPER('Quick Input')) > 0 THEN 1
      ELSE
      0
    END
      ) );
CREATE TEMP FUNCTION
  has_product_search(event_cat STRING,
    event_act STRING) AS ((
    SELECT
      CASE
        WHEN STRPOS(UPPER(event_cat), UPPER('Search')) > 0 AND STRPOS(UPPER(event_act), UPPER('Product Search')) > 0 THEN 1
      ELSE
      0
    END
      ) );
CREATE TEMP FUNCTION
  has_product_view(event_cat STRING,
    event_act STRING) AS ((
    SELECT
      CASE
        WHEN STRPOS(UPPER(event_cat), UPPER('Ecomm')) > 0 AND STRPOS(UPPER(event_act), UPPER('View Product Detail')) > 0 THEN 1
      ELSE
      0
    END
      ) );
CREATE TEMP FUNCTION
  has_order_event(event_cat STRING,
    event_act STRING) AS ((
    SELECT
      CASE
        WHEN STRPOS(UPPER(event_cat), UPPER('Ecomm')) > 0 AND STRPOS(UPPER(event_act), UPPER('Order Success')) > 0 THEN 1
        WHEN STRPOS(UPPER(event_cat), UPPER('Download')) > 0
      AND STRPOS(UPPER(event_act), UPPER('Backlog')) > 0 THEN 1
        WHEN STRPOS(UPPER(event_cat), UPPER('Download')) > 0 AND STRPOS(UPPER(event_act), UPPER('Invoices')) > 0 THEN 1
        WHEN STRPOS(UPPER(event_cat), UPPER('Order')) > 0
      AND STRPOS(UPPER(event_act), UPPER('Push')) > 0 THEN 1
        WHEN STRPOS(UPPER(event_cat), UPPER('Order')) > 0 AND STRPOS(UPPER(event_act), UPPER('Pull')) > 0 THEN 1
        WHEN STRPOS(UPPER(event_cat), UPPER('Order')) > 0
      AND STRPOS(UPPER(event_act), UPPER('Return Submit')) > 0 THEN 1
        WHEN STRPOS(UPPER(event_cat), UPPER('Order')) > 0 AND STRPOS(UPPER(event_act), UPPER('Shipment Track')) > 0 THEN 1
      ELSE
      0
    END
      ) );


CREATE OR REPLACE TABLE
  `myarrow_dev_revamp.billto_activity` AS
select * from (
SELECT
  sess.account_number__c AS unity_billto,
  sess.ga_date,
  sess.email,
  sess.registrationDate,
  ifnull(SUM(pv.pageview_activity_cnt + evt.event_activity_cnt),
    0) AS daily_activity_cnt,
  ifnull(SUM(reg_event_cnt),
    0) AS reg_activity_cnt,
  ifnull(SUM(quote_event_cnt + quote_view_cnt),
    0) AS quote_activity_cnt,
  ifnull(SUM(order_event_cnt + order_view_cnt + forecast_view_cnt),
    0) AS order_activity_cnt,
  ifnull(SUM(bom_event_cnt),
    0) AS bom_activity_cnt,
  ifnull(SUM(product_search_cnt + product_view_cnt + trade_cmp_cnt),
    0) AS product_activity_cnt,
	ifnull(sum(quote_view_cnt),0) 	AS	quote_view_cnt, 
	ifnull(sum(order_view_cnt),0) 	AS	order_view_cnt, 
	ifnull(sum(forecast_view_cnt),0) 	AS	forecast_view_cnt, 
	ifnull(sum(trade_cmp_cnt),0) 	AS	trade_cmp_cnt, 
	ifnull(sum(reg_event_cnt),0) 	AS	reg_event_cnt, 
	ifnull(sum(quote_event_cnt),0) 	AS	quote_event_cnt, 
	ifnull(sum(order_event_cnt),0) 	AS	order_event_cnt, 
	ifnull(sum(bom_event_cnt),0) 	AS	bom_event_cnt, 
	ifnull(sum(product_view_cnt),0) 	AS	product_view_cnt, 
	ifnull(sum(product_search_cnt),0)	AS	product_search_cnt 
FROM (
  SELECT
    usr.account_number__c,
    sess.internet_user_id,
	max(email) as email,
	max(registrationDate) as registrationDate,
    sess.session_id,
    sess.ga_date
    -- validate activity by sessions
  FROM
    `myarrow_dev.myarrow_user_list` usr
  JOIN
    `myarrow_dev.session_level_stats` sess
  ON
    CAST(usr.internetUserId AS int64) = CAST(sess.internet_user_id AS int64)
    group by usr.account_number__c,
    sess.session_id,
    sess.internet_user_id,
    sess.ga_date	
	) sess
  -- get page level stats data
LEFT JOIN (
  SELECT
    Session_Id,
    SUM(quote_view_cnt + order_view_cnt + forecast_cnt + trade_cmp_cnt) AS pageview_activity_cnt,
    SUM(quote_view_cnt) AS quote_view_cnt,
    SUM(order_view_cnt) AS order_view_cnt,
    SUM(forecast_cnt) AS forecast_view_cnt,
    SUM(trade_cmp_cnt) AS trade_cmp_cnt
  FROM
    `myarrow_dev.page_level_stats`
  GROUP BY
    session_id ) pv
ON
  sess.session_id = pv.session_id
LEFT JOIN (
    -- Event tracking including
  SELECT
    session_id,
    internet_user_id,
    SUM(reg_event + quote_event + order_event + bom_event + product_view + product_search) AS event_activity_cnt,
    SUM(reg_event) AS reg_event_cnt,
    SUM(quote_event) AS quote_event_cnt,
    SUM(order_event) AS order_event_cnt,
    SUM(bom_event) AS bom_event_cnt,
    SUM(product_view) AS product_view_cnt,
    SUM(product_search) AS product_search_cnt
  FROM (
    SELECT
      session_id,
      internet_user_id,
      has_registration_event(event_category,
        event_action) AS reg_event,
      has_quote_event(event_category,
        event_action) AS quote_event,
      has_order_event(event_category,
        event_action) AS order_event,
      has_bom_event(event_category,
        event_action) AS bom_event,
      has_product_view(event_category,
        event_action) AS product_view,
      has_product_search(event_category,
        event_action) AS product_search
    FROM
      `arrow-bigquery.myarrow_dev.event_level_stats` ) t
  GROUP BY
    session_id, internet_user_id ) evt
ON
  sess.session_id = evt.session_id and sess.internet_user_id = evt.internet_user_id
WHERE
  pv.pageview_activity_cnt > 0
  OR evt.event_activity_cnt > 0
GROUP BY
  sess.account_number__c,
  sess.ga_date,
  sess.email,
  sess.registrationDate
) d 
where unity_billto is not null
ORDER BY
  unity_billto DESC,
  ga_date;